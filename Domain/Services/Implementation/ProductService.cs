﻿using Domain.Responses;
using EcommerceService.Models;
using EcommerceService.Services.Contracts;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace EcommerceService.Services.Implementation
{
    public class ProductService: IProductService
    {
        private readonly IMongoCollection<Product> product;
        public ProductService()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("EcommerceSite");
            product = database.GetCollection<Product>("Products");
        }
        public CommandResponse CreateProduct(Product productPayload)
        {
            CommandResponse response = new CommandResponse();
            try
            {
                product.InsertOne(productPayload);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
            }
            return response;
        }
        public List<Product> GetProductList(string productType, string searchText, string[] productIds)
        {
            List<Product> result = new List<Product>();
            var filter = Builders<Product>.Filter.Where(x => x.ProductName.Contains(searchText));
            if (!string.IsNullOrEmpty(productType))
            {
                 filter&= Builders<Product>.Filter.Eq(x => x.ProductType, productType);
            }
            if(productIds.Length > 0)
            {
                filter &= Builders<Product>.Filter.In("Id", productIds);
            }
            result = product.Find<Product>(filter).ToList();
            return result;
        }
        public CommandResponse UpdateProduct(Product productdata)
        {
            CommandResponse response = new CommandResponse();
            try
            {
                product.ReplaceOneAsync<Product>(x => x.Id == productdata.Id, productdata);
                response.Success = true;
                response.StatusCode = 0;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                response.Success = true;
                response.StatusCode = 1;
                return response;
            }
            return response;
        }
    }
}
