﻿using Domain.Models;
using Domain.Queries;
using Domain.Responses;
using Domain.Services.Contracts;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Services.Implementation
{
   public class ProductOrderService: IProductOrderService
    {
        private readonly IMongoCollection<ProductOrder> productOrder;
        public ProductOrderService()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("EcommerceSite");
            productOrder = database.GetCollection<ProductOrder>("Orders");
        }
        public CommandResponse CreateProductOrder(ProductOrder productOrderData)
        {
            CommandResponse response = new CommandResponse();
            try
            {
                productOrder.InsertOne(productOrderData);
                response.Success = true;
                return response;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                response.Success = false;
            }
            return response;
        }
        public List<ProductOrder> GetProductOrderList(GetOrderQuery query)
        {
            List<ProductOrder> orderList = new List<ProductOrder>();
            var filter = Builders<ProductOrder>.Filter.Where(x => x.UserId == query.UserId);
            if (!string.IsNullOrEmpty(query.Status))
            {
                filter &= Builders<ProductOrder>.Filter.Eq(x => x.Status, query.Status);
            }
            if (query.OrderDate != null)
            {
                filter &= Builders<ProductOrder>.Filter.Eq(x => x.OrderDate, query.OrderDate);
            }
            if (query.DeliveryDate != null)
            {
                filter &= Builders<ProductOrder>.Filter.Eq(x => x.DeliveryDate, query.DeliveryDate);
            }
            orderList = productOrder.Find<ProductOrder>(filter).ToList();
            return orderList;
        }
    }
}
