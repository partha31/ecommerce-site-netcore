﻿using Domain.Models;
using Domain.Services.Contracts;
using EcommerceService.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Implementation
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IMongoCollection<ApplicationUser> userContext;
        private readonly SignInManager<ApplicationUser> signInManager;
        private UserManager<ApplicationUser> userManager;

        public UserService(
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IOptions<AppSettings> appSettings)
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("EcommerceSite");
            userContext = database.GetCollection<ApplicationUser>("applicationUsers");
            _appSettings = appSettings.Value;
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model, string ipAddress)
        {
            var result = await signInManager.PasswordSignInAsync(model.Username, model.Password, true, true);
            var user = await (Task<ApplicationUser>)userManager.FindByNameAsync(model.Username);

            // return null if user not found
            if (!result.Succeeded) return null;

            // authentication successful so generate jwt and refresh tokens
            var jwtToken = generateJwtToken(user);
            var refreshToken = generateRefreshToken(ipAddress);

            // save refresh token
            if(user.RefreshTokens == null)
            {
                user.RefreshTokens = new List<RefreshToken>();
            }
            user.RefreshTokens.Add(refreshToken);
            await userManager.UpdateAsync(user);
           // _context.SaveChanges();

            return new AuthenticateResponse(user, jwtToken, refreshToken.Token);
        }

        public AuthenticateResponse RefreshToken(string token, string ipAddress)
        {
            var filter = Builders<ApplicationUser>.Filter.ElemMatch(x => x.RefreshTokens, x => x.Token == token);
            var user = userContext.Find(filter).FirstOrDefault();

            // return null if no user found with token
            if (user == null) return null;

            var refreshToken = user.RefreshTokens.Find(x => x.Token == token);

            // return null if token is no longer active
            if (!refreshToken.IsActive) return null;

            // replace old refresh token with a new one and save
            var newRefreshToken = generateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;
            user.RefreshTokens.Add(newRefreshToken);
            userManager.UpdateAsync(user);
            //_context.SaveChanges();

            // generate new jwt
            var jwtToken = generateJwtToken(user);

            return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token);
        }

        public bool RevokeToken(string token, string ipAddress)
        {
            var filter = Builders<ApplicationUser>.Filter.ElemMatch(x => x.RefreshTokens, x => x.Token == token);
            var user = userContext.Find(filter).FirstOrDefault();

            // return false if no user found with token
            if (user == null) return false;

            var refreshToken = user.RefreshTokens.FindLast(x => x.Token == token);

            // return false if token is not active
            if (!refreshToken.IsActive) return false;

            // revoke token and save
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            userManager.UpdateAsync(user);
            //_context.SaveChanges();

            return true;
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            var filter = Builders<ApplicationUser>.Filter;
            return userContext.Find<ApplicationUser>(x => true).ToList();
        }

        public ApplicationUser GetById(int id)
        {
            return userContext.Find<ApplicationUser>(x => x.ItemId == id).FirstOrDefault();
        }

        // helper methods

        private string generateJwtToken(ApplicationUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken generateRefreshToken(string ipAddress)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }
    }
}
