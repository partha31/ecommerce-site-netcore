﻿using Domain.Models;
using Domain.Queries;
using Domain.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Services.Contracts
{
   public interface IProductOrderService
    {
        CommandResponse CreateProductOrder(ProductOrder productOrderData);

        List<ProductOrder> GetProductOrderList(GetOrderQuery query);
    }
}
