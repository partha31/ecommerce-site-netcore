﻿using Domain.Responses;
using EcommerceService.Models;
using System.Collections.Generic;

namespace EcommerceService.Services.Contracts
{
   public interface IProductService
    {
        CommandResponse CreateProduct(Product product);
        CommandResponse UpdateProduct(Product product);
        List<Product> GetProductList(string productType, string searchText, string[] productIds);
    }
}
