﻿using Domain.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Queries
{
   public class GetOrderQuery: IRequest<GetOrderResponse>
    {
        public string UserId { get; set; }
        public string Status { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
    }
}
