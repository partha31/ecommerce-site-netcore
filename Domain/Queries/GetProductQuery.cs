﻿using Domain.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Queries
{
   public class GetProductQuery : IRequest<GetProductResponse>
    {
       public string ProductType { get; set; }
        public string SearchText { get; set; }
        public string[] ProductIds { get; set; }
    }
}
