﻿using EcommerceService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Responses
{
   public class GetProductResponse
    {
       public List<Product> Products { get; set; }
       public int ProductCount { get; set; }
       public bool Success { get; set; }
    }
}
