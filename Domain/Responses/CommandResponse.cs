﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Responses
{
   public class CommandResponse
    {
       public bool Success { get; set; }
       public int StatusCode { get; set; }
       public object data { get; set; }
    }
}
