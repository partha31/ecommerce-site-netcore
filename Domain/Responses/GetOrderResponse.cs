﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Responses
{
   public class GetOrderResponse
    {
        public List<ProductOrder> ProductOrders { get; set; }
        public int ProductOrderCount { get; set; }
        public bool Success { get; set; }
    }
}
