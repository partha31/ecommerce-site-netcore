﻿using Domain.Commands;
using Domain.Models;
using Domain.Responses;
using Domain.Services.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.CommandHandlers
{
    public class ProductOrderCreateCommandHandler : IRequestHandler<ProductOrderCreateCommand, CommandResponse>
    {
        private readonly IProductOrderService productOrederService;
        public ProductOrderCreateCommandHandler(IProductOrderService productOrederService)
        {
            this.productOrederService = productOrederService;
        }
        public async Task<CommandResponse> Handle(ProductOrderCreateCommand command, CancellationToken cancellationToken)
        {
            CommandResponse response = new CommandResponse();
            try
            {
                ProductOrder orderData = new ProductOrder()
                {
                    Id = command.Id,
                    UserId = command.UserId,
                    ContactNumber = command.ContactNumber,
                    Status = command.Status,
                    ProductDatas = command.ProductDatas,
                    TotalPrice = command.TotalPrice,
                    DeliveryAddress = command.DeliveryAddress,
                    DeliveryMethod = command.DeliveryMethod,
                    PaymentMethod = command.PaymentMethod,
                    PaymentStatus = command.PaymentStatus,
                    OrderDate = command.OrderDate,
                    DeliveryDate = command.DeliveryDate
                };
                response = productOrederService.CreateProductOrder(orderData);
                response.StatusCode = 0;
                return response;
            }
            catch(Exception ex)
            {
                response.StatusCode = 1;
                response.Success = false;
            }
            return response;
        }
    }
}
