﻿using Domain.Commands;
using Domain.Responses;
using EcommerceService.Models;
using EcommerceService.Services.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.CommandHandlers
{
   public class ProductUpdateCommandHandler : IRequestHandler<ProductUpdateCommand, CommandResponse>
    {
        private readonly IProductService productservice;
        public ProductUpdateCommandHandler(IProductService productservice)
        {
            this.productservice = productservice;
        }

        public async Task<CommandResponse> Handle(ProductUpdateCommand command, CancellationToken cancellationToken)
        {
            CommandResponse response = new CommandResponse();
            Product productData = new Product()
            {
                ProductName = command.ProductName,
                ProductAmount = command.ProductAmount,
                ProductBrand = command.ProductBrand,
                ProductDescription = command.ProductDescription,
                ProductImageUrl = command.ProductImageUrl,
                ProductPrice = command.ProductPrice,
                ProductQuantity = command.ProductQuantity,
                ProductType = command.ProductType,
                Id = command.Id
            };
            productservice.UpdateProduct(productData);
            return response;
        }
    }
}
