﻿using Domain.Commands;
using Domain.Responses;
using EcommerceService.Models;
using EcommerceService.Services.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.CommandHandlers
{
   public class ProjectCreateCommandHandler : IRequestHandler<ProjectCreateCommand, CommandResponse>
    {
        private readonly IProductService productService;
        public ProjectCreateCommandHandler(IProductService productService)
        {
            this.productService = productService;
        }

        public async Task<CommandResponse> Handle(ProjectCreateCommand command, CancellationToken cancellationToken)
        {
            CommandResponse response = new CommandResponse();
            Product productData = new Product() {
                ProductName = command.ProductName,
                ProductAmount = command.ProductAmount,
                ProductBrand = command.ProductBrand,
                ProductDescription = command.ProductDescription,
                ProductImageUrl = command.ProductImageUrl,
                ProductPrice = command.ProductPrice,
                ProductQuantity = command.ProductQuantity,
                ProductType = command.ProductType,
                Id = command.Id
            };
            response = productService.CreateProduct(productData);
            if(response.Success)
            {
                response.StatusCode = 0;
            }
            return response;
        }
    }
}
