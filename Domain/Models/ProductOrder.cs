﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
   public class ProductOrder
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ContactNumber { get; set; }
        public string Status { get; set; }
        public string TotalPrice { get; set; }
        public ProductData[] ProductDatas { get; set; }
        public string DeliveryMethod { get; set; }
        public string DeliveryAddress { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}
