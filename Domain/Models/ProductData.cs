﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
   public class ProductData
    {
        public string ProductId { get; set; }
        public string Amount { get; set; }
    }
}
