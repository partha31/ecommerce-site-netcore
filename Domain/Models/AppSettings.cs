﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
