﻿using Domain.Queries;
using Domain.Responses;
using EcommerceService.Services.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.QueryHandlers
{
   public class GetProductQueryHandler : IRequestHandler<GetProductQuery, GetProductResponse>
    {
        private readonly IProductService productService;
        public GetProductQueryHandler(IProductService productService)
        {
            this.productService = productService;
        }

        public async Task<GetProductResponse> Handle(GetProductQuery query, CancellationToken cancellationToken)
        {
            GetProductResponse response = new GetProductResponse();
            var productType = query.ProductType;
            var searchText = query.SearchText;
            var productIds = query.ProductIds;
            try
            {
                response.Products = productService.GetProductList(productType, searchText, productIds);
                response.Success = true;
                response.ProductCount = response.Products.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                response.Products = null;
                response.Success = false;
                response.ProductCount = 0;
                return response;
            }
            return response;
        }
    }
}
