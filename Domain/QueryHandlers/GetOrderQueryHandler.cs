﻿using Domain.Queries;
using Domain.Responses;
using Domain.Services.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.QueryHandlers
{
   public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, GetOrderResponse>
    {
        private readonly IProductOrderService productOrederService;
        public GetOrderQueryHandler(IProductOrderService productOrederService)
        {
            this.productOrederService = productOrederService;
        }
        public async Task<GetOrderResponse> Handle(GetOrderQuery query, CancellationToken cancellationToken)
        {
            GetOrderResponse response = new GetOrderResponse();
            try
            {
                response.ProductOrders = productOrederService.GetProductOrderList(query);
                response.ProductOrderCount = response.ProductOrders.Count;
                response.Success = true;
                return response;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                response.Success = false;
            }
            return response;
        }
    }
}
