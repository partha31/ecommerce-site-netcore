﻿using Domain.Responses;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Commands
{
   public class ProductUpdateCommand: IRequest<CommandResponse>
    {
        [BsonId]
        public string Id { get; set; }
        [BsonElement("ProductName")]
        public string ProductName { get; set; }
        [BsonElement("ProductType")]
        public string ProductType { get; set; }
        [BsonElement("ProductPrice")]
        public string ProductPrice { get; set; }
        [BsonElement("ProductDescription")]
        public string ProductDescription { get; set; }
        [BsonElement("ProductImageUrl")]
        public string ProductImageUrl { get; set; }
        [BsonElement("ProductQuantity")]
        public Int64 ProductQuantity { get; set; }
        [BsonElement("ProductAmount")]
        public Int64 ProductAmount { get; set; }
        [BsonElement("ProductBrand")]
        public string ProductBrand { get; set; }
    }
}
