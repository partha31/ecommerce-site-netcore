﻿using Domain.Models;
using Domain.Responses;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Commands
{
   public class ProductOrderCreateCommand : IRequest<CommandResponse>
    {
        [BsonId]
        public string Id { get; set; }
        [BsonElement("UserId")]
        public string UserId { get; set; }
        [BsonElement("ContactNumber")]
        public string ContactNumber { get; set; }
        [BsonElement("Status")]
        public string Status { get; set; }
        [BsonElement("TotalPrice")]
        public string TotalPrice { get; set; }
        [BsonElement("ProductDatas")]
        public ProductData[] ProductDatas { get; set; }
        [BsonElement("DeliveryMethod")]
        public string DeliveryMethod { get; set; }
        [BsonElement("DeliveryAddress")]
        public string DeliveryAddress { get; set; }
        [BsonElement("PaymentMethod")]
        public string PaymentMethod { get; set; }
        [BsonElement("PaymentStatus")]
        public string PaymentStatus { get; set; }
        [BsonElement("OrderDate")]
        public DateTime OrderDate { get; set; }
        [BsonElement("DeliveryDate")]
        public DateTime DeliveryDate { get; set; }
    }
}
