﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcommerceService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EcommerceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
        private UserManager<ApplicationUser> userManager;
        private SignInManager<ApplicationUser> signInManager;
        public ApplicationUserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<object> CreateUser(ApplicationUserModel userModel)
        {
            var applicationUser = new ApplicationUser()
            {
                UserName = userModel.UserName,
                PhoneNumber = userModel.PhoneNumber,
                Email = userModel.Email
            };
            try
            {
                var result = await userManager.CreateAsync(applicationUser, userModel.Password);
                if (result.Succeeded)
                {
                    await signInManager.PasswordSignInAsync(userModel.UserName, userModel.Password, true, true);
                }
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [Route("Login")]
        public async Task<object> LoginUser(ApplicationUserModel userModel)
        {
            try
            {
                var result = await signInManager.PasswordSignInAsync(userModel.UserName, userModel.Password, true, true);
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [Route("Logout")]
        public async Task<object> LogoutUser(string redirectUrl)
        {
            try
            {
                await signInManager.SignOutAsync();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
                return false;
            }
        }
    }
}