﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Commands;
using Domain.Responses;
using EcommerceService.Models;
using EcommerceService.Services.Contracts;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EcommerceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcommerceSiteCommandController : ControllerBase
    {
        private readonly IProductService productService;
        private readonly IMediator mediator;
        public EcommerceSiteCommandController(IProductService productService,
            IMediator mediator)
        {
            this.productService = productService;
            this.mediator = mediator;
        }
        [HttpPost("CreateProduct")]
        public async Task<CommandResponse> CreateProduct([FromBody]ProjectCreateCommand command)
        {

            return await this.mediator.Send(command);
        }
        [HttpPost("UpdateProduct")]
        public async Task<CommandResponse> UpdateProduct([FromBody]ProductUpdateCommand command)
        {
            return await mediator.Send(command);
        }
        [HttpPost("CreateProductOrder")]
        public async Task<CommandResponse> CreateProductOrder([FromBody]ProductOrderCreateCommand command)
        {
            return await mediator.Send(command);
        }
    }
}