﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Queries;
using Domain.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EcommerceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcommerceSiteQueryController : ControllerBase
    {
        private readonly IMediator mediator;
        public EcommerceSiteQueryController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpPost("GetProduct")]
        public async Task<GetProductResponse> GetProduct([FromBody]GetProductQuery query)
        {
            return await mediator.Send(query);
        }
        [HttpPost("GetOrderList")]
        public async Task<GetOrderResponse> GetOrderList([FromBody]GetOrderQuery query)
        {
            return await mediator.Send(query);
        }
       
     }
}